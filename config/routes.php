<?php

// config/routes.php
use Symfony\Component\Routing\Loader\Configurator\RoutingConfigurator;
use Symfony\Bundle\FrameworkBundle\Controller\RedirectController;
use App\Controller\PosterController;
use App\Controller\UserController;
use App\Controller\BookController;
use App\Controller\CategoryController;
use App\Controller\AuthorController;

// php bin/console debug:router
return function (RoutingConfigurator $routes) {
    $controllerIds = [CategoryController::class, BookController::class, PosterController::class, AuthorController::class];
    foreach ($controllerIds as $controllerId) {
        $resource = (new $controllerId())->resource();
        $routes->add("{$resource}_list", "/$resource/list")
            ->controller([$controllerId, 'list'])
            ->methods(['GET']);
        $routes->add("{$resource}_single", "/$resource/single/{id}")
            ->controller([$controllerId, 'single'])
            ->methods(['GET'])
            ->requirements(['id' => '\d+']);
        $routes->add("{$resource}_insert", "/$resource/insert")
            ->controller([$controllerId, 'insert'])
            ->methods(['POST']);
        $routes->add("{$resource}_edit", "/$resource/{id}/edit")
            ->controller([$controllerId, 'edit'])
            ->methods(['POST'])
            ->requirements(['id' => '\d+']);
        $routes->add("{$resource}_delete", "/$resource/{id}/delete")
            ->controller([$controllerId, 'delete'])
            ->methods(['POST'])
            ->requirements(['id' => '\d+']);
    }
    $routes->add('homepage', '/')
        ->controller(RedirectController::class)
         ->defaults([
            'route' => 'app_books'
        ])
    ;
};

/* return function (Symfony\Component\Routing\RouteCollectionBuilder $routes) {
    $controllerIds = [CategoryController::class, BookController::class, PosterController::class, AuthorController::class];
    foreach ($controllerIds as $controllerId) {
        $resource = (new $controllerId())->resource();
        $routes->add("/$resource/list","$controllerId::list" ,"{$resource}_list")
            ->setMethods(['GET']);
        $routes->add("/$resource/single/{id}", "$controllerId::single", "{$resource}_single", )
            ->setMethods(['GET'])
            ->setRequirements(['id' => '\d+']);
        $routes->add("$controllerId::insert", "/$resource/insert", "{$resource}_insert")
            ->setMethods(['POST']);
        $routes->add("/$resource/{id}/edit", "$controllerId::edit", "{$resource}_edit")
            ->setMethods(['POST'])
            ->setRequirements(['id' => '\d+']);
        $routes->add("/$resource/{id}/delete", "$controllerId::delete", "{$resource}_delete")
            ->setMethods(['POST'])
            ->setRequirements(['id' => '\d+']);
    }
}; */