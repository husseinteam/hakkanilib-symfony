/*
-- Query: SELECT * FROM reader_db.user
LIMIT 0, 1000

-- Date: 2021-02-05 11:01
*/
INSERT INTO `` (`id`,`email`,`identity`,`api_token`,`roles`,`password`,`api_token_generation_time`,`generated_at`,`updated_at`) VALUES (11,'ugurbostan@hakkani.org','ugur-bostan',NULL,'[\"ROLE_ADMIN\"]','$2y$13$8SQOyY3Zn6LqpS/CqEevZeCKvg5mKPAETuCWdQIiV6Y31n.NYbODW',NULL,'2021-02-05 07:52:26',NULL);
INSERT INTO `` (`id`,`email`,`identity`,`api_token`,`roles`,`password`,`api_token_generation_time`,`generated_at`,`updated_at`) VALUES (12,'hussein.sonmez@outlook.com','25871077074',NULL,'[\"ROLE_ADMIN\"]','$2y$13$YO2VoP0bVxqG965jSUyW..5j9mBhNRJ.EyuySgXCBt0JwlUECshqi',NULL,'2021-02-05 07:52:26',NULL);
INSERT INTO `user` (`id`,`email`,`identity`,`api_token`,`roles`,`password`,`api_token_generation_time`,`generated_at`,`updated_at`) VALUES (13,'destek@natro.com','restricted_user',NULL,'[\"ROLE_USER\"]','$2y$13$f41UDwPDJqbSsp3jGvV7juj1JzCS9lpxj7HrQGP08FW4zw96BmI1q',NULL,'2021-02-05 07:52:26',NULL);
