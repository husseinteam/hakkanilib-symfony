function disableCopy() {
  // disable right click
  $(function() {
    $(this).bind("contextmenu", function(e) {
        e.preventDefault();
    });
  }); 

  // Prevent F12      
  $(document).keydown(function (event) {
    if (event.keyCode == 123) { // Prevent F12
        return false;
    } else if (event.ctrlKey && event.shiftKey && event.keyCode == 73) { // Prevent Ctrl+Shift+I        
        return false;
    }
  });

  //stop copy of content
  function killCopy(e){
    return false;
  }
  function reEnable(){
    return true;
  }
  document.onselectstart=new Function ("return false");
  if (window.sidebar){
    document.onmousedown=killCopy;
    document.onclick=reEnable;
  }

  // prevent save
  $(document).bind('keydown keypress', 'ctrl+s', function(e) {
    e.cancelBubble = true;
    e.preventDefault();
    e.stopImmediatePropagation();
    return false;
  });

  // prevent print
  window.print=new Function ("return false");
  $(document).bind('keydown keypress', 'ctrl+p', function(e) {
    e.cancelBubble = true;
    e.preventDefault();
    e.stopImmediatePropagation();
    return false;
  });
  
} 
document.addEventListener('DOMContentLoaded', disableCopy, true);