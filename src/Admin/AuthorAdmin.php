<?php

namespace App\Admin;

use App\Entity\Category;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Sonata\AdminBundle\Show\ShowMapper;

final class AuthorAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper): void
    {
        $formMapper
        ->with('Content', ['class' => 'col-md-12', 'label' => 'label.admin.author_info', 'box_class' => 'box box-solid box-primary',])
        ->add('full_name', TextType::class, [
            'label' => 'label.admin.author_full_name',
        ]);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper): void
    {
        $datagridMapper->add('full_name');
    }

    protected function configureListFields(ListMapper $listMapper): void
    {
        $listMapper->addIdentifier('full_name', TextType::class, [
            'label' => 'label.admin.author_full_name',
        ]);
    }

    protected function configureShowFields(ShowMapper $showMapper): void
    {
        $showMapper
            //->tab('General') // the tab call is optional
            ->with('Categories', [
                'class'       => 'col-md-12',
                'box_class'   => 'box box-solid box-primary',
                'label' => 'label.admin.author_info',
            ])
            ->add('full_name', TextType::class, [
                'label' => 'label.admin.author_full_name',
            ])
            // ...
            ->end()
            //->end()
        ;
    }

    public function preUpdate($entity): void
    {
        $entity->setUpdatedAt(new \DateTimeImmutable());
    }

    protected $classnameLabel = 'Kitap Yazari';

}