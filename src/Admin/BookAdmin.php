<?php

namespace App\Admin;

use App\Entity\Author;
use App\Entity\Book;
use App\Entity\Category;
use App\Entity\Poster;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Validator\Constraints\File;
use Sonata\AdminBundle\Form\Type\ModelType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Sonata\AdminBundle\Show\ShowMapper;
use Vich\UploaderBundle\Form\Type\VichFileType;

final class BookAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper): void
    {
        $formMapper
            ->with('Content', [
                'class' => 'col-md-9',
                'label' => 'label.admin.book_info', 
                'box_class' => 'box box-solid box-primary',
            ])
            ->add('title', TextType::class, [
                'label' => 'label.admin.book_title',
            ])
            ->add('poster', ModelType::class, [
                'class' => Poster::class,
                'property' => 'posterOriginalName',
                'label' => 'label.admin.book_poster',
            ])
            ->add('pdfFile', VichFileType::class, [
                'label' => 'label.admin.book_data',
                'required' => $this->isCurrentRoute('create'),
                'download_link' => $this->isCurrentRoute('edit'),
                'allow_delete' => false,
                'constraints' => [
                    new File([
                        'maxSize' => '102400k',
                        'mimeTypes' => [
                            'application/pdf',
                            'application/x-pdf',
                        ],
                        'mimeTypesMessage' => "PDF değil ya da 100MB'dan büyük",
                    ])
                ],
            ])
            ->add('pdfOriginalName', TextType::class, [
                'label' => 'label.admin.pdf_name',
                'disabled' => true
            ])
            ->add('next', ModelType::class, [
                'class' => Book::class,
                'property' => 'pdfOriginalName',
                'label' => 'label.admin.book_next',
                'required' => false,
                'btn_add' => false,
            ])
            ->add('published', CheckboxType::class, [
                'label' => 'label.admin.book_published',
                'required' => false
            ])
            ->add('rtl', CheckboxType::class, [
                'label' => 'label.admin.book_rtl',
                'required' => false
            ])
            ->end()
            ->with('Meta data', [
                'class' => 'col-md-3',
                'box_class' => 'box box-solid box-success',
                'label' => 'label.admin.book_metadata'
            ])
            ->add('category', ModelType::class, [
                'class' => Category::class,
                'property' => 'title',
                'label' => 'label.admin.book_category',
            ])
            ->add('author', ModelType::class, [
                'class' => Author::class,
                'property' => 'full_name',
                'label' => 'label.admin.book_author',
            ])
            ->end();
    }

    protected function configureListFields(ListMapper $listMapper): void
    {
        $listMapper
            ->addIdentifier('title', TextType::class, [
                'label' => 'label.admin.book_title',
            ])
            ->add('author', null, [
                'label' => 'label.admin.book_author',
            ])
            ->add('published', null, [
                'label' => 'label.admin.book_published',
            ])
            ->add('rtl', null, [
                'label' => 'label.admin.book_rtl',
            ])
            ->add('next', EntityType::class, [
                'class' => Book::class,
                'choice_label' => 'pdfOriginalName',
                'label' => 'label.admin.book_next',
            ])
            ->add('poster', EntityType::class, [
                'class' => Poster::class,
                'choice_label' => 'posterOriginalName',
                'label' => 'label.admin.book_poster',
            ])
            ->add('category', EntityType::class, [
                'class' => Category::class,
                'choice_label' => 'title',
                'label' => 'label.admin.book_category',
            ]);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper): void
    {
        $datagridMapper
            ->add('title')
            ->add('author')
            ->add('category');
    }

    protected function configureShowFields(ShowMapper $showMapper): void
    {
        $showMapper
            ->with('Content', [
                'class' => 'col-md-9', 
                'label' => 'label.admin.book_info', 
                'box_class' => 'box box-solid box-primary',
            ])
            ->add('title', TextType::class, [
                'label' => 'label.admin.book_title',
            ])
            ->add('pdfOriginalName', TextType::class, [
                'label' => 'label.admin.pdf_name',
            ])
            ->add('poster', ModelType::class, [
                'class' => Poster::class,
                'property' => 'posterOriginalName',
                'label' => 'label.admin.book_poster',
            ])
            ->add('next', ModelType::class, [
                'class' => Book::class,
                'property' => 'pdfOriginalName',
                'label' => 'label.admin.book_next',
            ])
            ->add('published', null, [
                'label' => 'label.admin.book_published',
            ])
            ->add('rtl', null, [
                'label' => 'label.admin.book_rtl',
            ])
            ->end()
            ->with('Meta data', [
                'class' => 'col-md-3',
                'box_class' => 'box box-solid box-success',
                'label' => 'label.admin.book_metadata'
            ])
            ->add('category', ModelType::class, [
                'class' => Category::class,
                'property' => 'title',
                'label' => 'label.admin.book_category',
            ])
            ->add('author', ModelType::class, [
                'class' => Author::class,
                'property' => 'full_name',
                'label' => 'label.admin.book_author',
            ])
            ->end();
    }

    protected function getHelpFileFormOptions($mergeWithArray = null): array
    {
        if ($this->hasParentFieldDescription()) { // this Admin is embedded
            // $getter will be something like 'getlogoImage'
            $getter = 'get' . $this->getParentFieldDescription()->getFieldName();

            // get hold of the parent object
            $parent = $this->getParentFieldDescription()->getAdmin()->getSubject();
            if ($parent) {
                $image = $parent->$getter();
            } else {
                $image = null;
            }
        } else {
            $image = $this->getSubject();
        }

        // use $fileFormOptions so we can add other options to the field
        $fileFormOptions = ['required' => false];
        if ($image && ($webPath = $image->getPdfName())) {
            // add a 'help' option containing the preview's img tag
            $fileFormOptions['help'] = '<img src="' . $webPath . '" class="admin-preview"/>';
            $fileFormOptions['help_html'] = true;
        }
        if ($mergeWithArray != null) {
            $fileFormOptions = array_merge($fileFormOptions, $mergeWithArray);
        }
        return $fileFormOptions;
    }

    public function prePersist($image): void
    {
        $this->manageFileUpload($image);
    }

    public function preUpdate($image): void
    {
        $this->manageFileUpload($image);
    }

    private function manageFileUpload($image)
    {
        $image->refreshUpdated();
    }

    protected $classnameLabel = 'Pdf Kitap';
}