<?php

namespace App\Entity;

use App\Repository\BookRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use App\Entity\GenericEntityTrait;

/**
 * @ORM\Entity(repositoryClass=BookRepository::class)
 * @Vich\Uploadable
 */
class Book
{
    use GenericEntityTrait;

    public function __construct()
    {
        $this->setGeneratedAt();
        $this->posters = new ArrayCollection();
    }
    
    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     * 
     * @Vich\UploadableField(mapping="book_pdf", fileNameProperty="pdfName", size="pdfSize", originalName="pdfOriginalName")
     * 
     * @var File|null
     */
    private $pdfFile;

    /**
     * @ORM\Column(type="string", nullable=true)
     *
     * @var string|null
     */
    private $pdfName;

    /**
     * @ORM\Column(type="string", nullable=true)
     *
     * @var string|null
     */
    private $pdfOriginalName;

    /**
     * @ORM\Column(type="integer", nullable=true)
     *
     * @var int|null
     */
    private $pdfSize;

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile|null $pdfFile
     */
    public function setPdfFile(?File $pdfFile = null): void
    {
        $this->pdfFile = $pdfFile;
    }

    public function refreshUpdated()
    {
        // It is required that at least one field changes if you are using doctrine
        // otherwise the event listeners won't be called and the file is lost
        $this->setUpdatedAt(new \DateTime());
    }

    public function getPdfFile(): ?File
    {
        return $this->pdfFile;
    }

    public function setPdfName(?string $pdfName): self
    {
        $this->pdfName = $pdfName;
        return $this;
    }

    public function getPdfName(): ?string
    {
        return $this->pdfName;
    }

    public function setPdfSize(?int $pdfSize): self
    {
        $this->pdfSize = $pdfSize;
        return $this;
    }

    public function getPdfSize(): ?int
    {
        return $this->pdfSize;
    }
    
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\ManyToOne(targetEntity=Category::class, inversedBy="books")
     * @ORM\JoinColumn(onDelete="CASCADE", nullable=false)
     * @JMS\MaxDepth(1)
     */
    private $category;

    /**
     * @ORM\ManyToOne(targetEntity=Author::class, inversedBy="books")
     * @ORM\JoinColumn(onDelete="CASCADE", nullable=false)
     * @JMS\MaxDepth(1)
     */
    private $author;

    /**
     * @ORM\OneToOne(targetEntity=Poster::class, inversedBy="book", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $poster;

    /**
     * @ORM\Column(type="boolean")
     */
    private $rtl;

    /**
     * @ORM\OneToOne(targetEntity=Book::class)
     * @JMS\MaxDepth(1)
     */
    private $next;

    /**
     * @ORM\Column(type="boolean")
     */
    private $published;

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }
  
    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getAuthor(): ?Author
    {
        return $this->author;
    }

    public function setAuthor(?Author $author): self
    {
        $this->author = $author;

        return $this;
    }

    public function getPdfOriginalName(): ?string
    {
        return $this->pdfOriginalName;
    }

    public function setPdfOriginalName(?string $pdfOriginalName): self
    {
        $this->pdfOriginalName = $pdfOriginalName;

        return $this;
    }
    
    public function getPoster(): ?Poster
    {
        return $this->poster;
    }

    public function setPoster(Poster $poster): self
    {
        $this->poster = $poster;

        return $this;
    }

    public function __toString()
    {
        return $this->title;
    }

    public function getRtl(): ?bool
    {
        return $this->rtl;
    }

    public function setRtl(bool $rtl): self
    {
        $this->rtl = $rtl;

        return $this;
    }

    public function getNext(): ?self
    {
        return $this->next;
    }

    public function setNext(?self $next): self
    {
        $this->next = $next;

        return $this;
    }

    public function getPublished(): ?bool
    {
        return $this->published;
    }

    public function setPublished(bool $published): self
    {
        $this->published = $published;

        return $this;
    }

}