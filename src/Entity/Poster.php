<?php

namespace App\Entity;

use App\Repository\PosterRepository;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use App\Entity\GenericEntityTrait;

/**
 * @ORM\Entity(repositoryClass=PosterRepository::class)
 * @Vich\Uploadable
 */
class Poster
{
    use GenericEntityTrait;

    public function __construct()
    {
        $this->setGeneratedAt();
    }
    
    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     * 
     * @Vich\UploadableField(mapping="book_poster", fileNameProperty="posterName", size="posterSize", originalName="posterOriginalName")
     * 
     * @var File|null
     */
    private $posterFile;

    /**
     * @ORM\Column(type="string", nullable=true)
     *
     * @var string|null
     */
    private $posterName;

    /**
     * @ORM\Column(type="integer", nullable=true)
     *
     * @var int|null
     */
    private $posterSize;

    /**
     * @ORM\OneToOne(targetEntity=Book::class, mappedBy="poster", cascade={"persist", "remove"})
     * @JMS\MaxDepth(0)
     */
    private $book;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $posterOriginalName;

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile|null $posterFile
     */
    public function setPosterFile(?File $posterFile = null): void
    {
        $this->posterFile = $posterFile;
    }

    public function refreshUpdated()
    {
        // It is required that at least one field changes if you are using doctrine
        // otherwise the event listeners won't be called and the file is lost
        $this->setUpdatedAt(new \DateTime());
    }

    public function getPosterFile(): ?File
    {
        return $this->posterFile;
    }

    public function setPosterName(?string $posterName): self
    {
        $this->posterName = $posterName;
        return $this;
    }

    public function getPosterName(): ?string
    {
        return $this->posterName;
    }

    public function setPosterSize(?int $posterSize): self
    {
        $this->posterSize = $posterSize;
        return $this;
    }

    public function getPosterSize(): ?int
    {
        return $this->posterSize;
    }

    public function getBook(): ?Book
    {
        return $this->book;
    }

    public function setBook(Book $book): self
    {
        // set the owning side of the relation if necessary
        if ($book->getPoster() !== $this) {
            $book->setPoster($this);
        }

        $this->book = $book;

        return $this;
    }

    public function __toString() {
        return $this->posterName;
    }

    public function getPosterOriginalName(): ?string
    {
        return $this->posterOriginalName;
    }

    public function setPosterOriginalName(?string $posterOriginalName): self
    {
        $this->posterOriginalName = $posterOriginalName;

        return $this;
    }

}