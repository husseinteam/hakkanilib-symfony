<?php

namespace App\Controller;

use App\Entity\Author;
use App\Entity\Book;
use App\Entity\Category;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PdfViewController extends AbstractController
{
    /**
     * @Route("/public/books/{page}", name="app_books", methods={"GET"}, defaults={"page": 1}, requirements={"page"="\d+"})
     */
    public function getBooksByPage(int $page = 1): Response
    {
        $em = $this->getDoctrine()->getManager();

        // build the query for the doctrine paginator
        $query = $em->createQueryBuilder()
            ->select("book, poster, author")
            ->from(Book::class, "book")
            ->leftJoin("book.poster", "poster")
            ->leftJoin("book.author", "author")
            ->getQuery();
        //set page size
        $pageSize = '10';

        // load doctrine Paginator
        $paginator = new Paginator($query);

        // you can get total items
        $totalItems = count($paginator);

        // get total pages
        $pagesCount = ceil($totalItems / $pageSize);

        // now get one page's items:
        $paginator
            ->getQuery()
            ->setFirstResult($pageSize * ($page - 1)) // set the offset
            ->setMaxResults($pageSize);
        $books = array();
        foreach ($paginator as $book) {
            $books[] = $book;
        }
        return $this->render('public/books.html.twig', [
            'books' => BookController::modifyBookList($books),
            'page' => $page,
            'pagesCount' => $pagesCount,
        ]);
    }

    /**
     * @Route("/public/books/author/{authorId}/{page}", name="app_authors", methods={"GET"}, defaults={"page": 1,"authorId": 0}, requirements={"page"="\d+","authorId"="\d+"})
     */
    public function getBooksByAuthor(int $authorId = 0, int $page = 1): Response
    {
        $em = $this->getDoctrine()->getManager();

        // build the query for the doctrine paginator
        $query = $em->createQueryBuilder()
            ->select("book, poster, author")
            ->from(Book::class, "book")
            ->where('author.id = ?1')
            ->leftJoin("book.poster", "poster")
            ->leftJoin("book.author", "author")
            ->setParameter('1', $authorId)
            ->getQuery();
        //set page size
        $pageSize = '10';

        // load doctrine Paginator
        $paginator = new Paginator($query);

        // you can get total items
        $totalItems = count($paginator);

        // get total pages
        $pagesCount = ceil($totalItems / $pageSize);

        // now get one page's items:
        $paginator
            ->getQuery()
            ->setFirstResult($pageSize * ($page - 1)) // set the offset
            ->setMaxResults($pageSize);
        $books = array();
        foreach ($paginator as $book) {
            $books[] = $book;
        }
        return $this->render('public/authors.html.twig', [
            'authors' => $em->getRepository(Author::class)->findAll(),
            'authorId' => $authorId,
            'books' => BookController::modifyBookList($books),
            'page' => $page,
            'pagesCount' => $pagesCount,
        ]);
    }

    /**
     * @Route("/public/books/category/{categoryId}/{page}", name="app_categories", methods={"GET"}, defaults={"page": 1,"categoryId": 0}, requirements={"page"="\d+","categoryId"="\d+"})
     */
    public function getBooksByCategory(int $categoryId = 0, int $page = 1): Response
    {
        $em = $this->getDoctrine()->getManager();

        // build the query for the doctrine paginator
        $query = $em->createQueryBuilder()
            ->select("book, poster, author, category")
            ->from(Book::class, "book")
            ->where('category.id = ?1')
            ->leftJoin("book.poster", "poster")
            ->leftJoin("book.author", "author")
            ->leftJoin("book.category", "category")
            ->setParameter('1', $categoryId)
            ->getQuery();
        //set page size
        $pageSize = '10';

        // load doctrine Paginator
        $paginator = new Paginator($query);

        // you can get total items
        $totalItems = count($paginator);

        // get total pages
        $pagesCount = ceil($totalItems / $pageSize);

        // now get one page's items:
        $paginator
            ->getQuery()
            ->setFirstResult($pageSize * ($page - 1)) // set the offset
            ->setMaxResults($pageSize);
        $books = array();
        foreach ($paginator as $book) {
            $books[] = $book;
        }
        return $this->render('public/categories.html.twig', [
            'categories' => $em->getRepository(Category::class)->findAll(),
            'categoryId' => $categoryId,
            'books' => BookController::modifyBookList($books),
            'page' => $page,
            'pagesCount' => $pagesCount,
        ]);
    }

    /**
     * @Route("/public/book/{id}/view", name="app_book", methods={"GET"}, requirements={"id"="\d+"})
     */
    public function getBook($id): Response
    {
        $em = $this->getDoctrine()->getManager();
        $booksRepo = $em->getRepository(Book::class);
        $book = $booksRepo->findOneBy(['id' => $id]);
        $root = $this->get('kernel')->getProjectDir();
        $pdfPath = $root . '/assets/books/' . $book->getPdfName();
        $parameters = [
            //Tell to the bundle that the pdf is outside the webroot
            'isPdfOutsideWebroot' => true,
            //Tell to the bundle where is the pdf. (absolute path for outside temporal folder pdf, just the <name>.pdf for inside temporal folder)
            'pdf' => $pdfPath,
            //Tell to the bundle that its necessary to delete pdf after render.
            'deletePdfInTmpAfterRenderized' => false,
            'showToolBar' => true,
            'showLeftToolbarButton' => true,
            'showSearchInDocumentButton' => true,
            'showPreviousPageButton' => true,
            'showPreviousPageButton' => true,
            'showFindPageInputText' => true,
            'showNumberOfPagesLabel' => true,
            'showZoomInButton' => false,
            'showZoomOutButton' => false,
            'showScaleSelectComboBox' => false,
            'showPresentationModeButton' => true,
            'showOpenFileButton' => false,
            'showPrintButton' => false,
            'showDownloadButton' => false,
            'showViewBookmarkButton' => true,
            'showToolsButton' => true,
        ];
        return $this->get('jjalvarezl_pdfjs_viewer.viewer_controller')->renderCustomViewer($parameters);
    }
}
