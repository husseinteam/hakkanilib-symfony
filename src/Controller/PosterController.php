<?php

namespace App\Controller;

use App\Engines\FileEngine;
use App\Entity\Poster;
use App\Repository\BookRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use Doctrine\ORM\EntityManagerInterface;
use Vich\UploaderBundle\Handler\DownloadHandler;

class PosterController extends GenericAbstractController
{
    public function __construct(EntityManagerInterface $entityManager = null, TranslatorInterface $translator = null)
    {
        if ($entityManager != null && $translator != null) {
            parent::__construct($entityManager, $translator);
        }
    }
    function class()
    {
        return Poster::class;
    }
    function resource()
    {
        return 'posters';
    }
    function fill(&$wireframe, $json)
    {
        $book = $this->container->get(BookRepository::class)->find($json['book_id']);
        $wireframe->setData($json['data'])
            ->setPosterName($json['posterName'])
            ->setServerPath($json['server_path'])
            ->setBook($book);
    }

    public function index(): Response
    {
        return $this->render('poster/index.html.twig', [
            'controller_name' => 'PosterController',
        ]);
    }

    /**
    * @Route("/download/poster", name="poster_download", methods={"POST"})
    */
    public function downloadPoster(DownloadHandler $downloadHandler, Request $request): Response
    {
        $json = (array) $request->request->all();
        $thumb = $json['isThumbnail'];
        $poster = $this->repo->findOneBy(['posterName' => $json['posterName']]);
        if($thumb == true) {
            $root = $this->container->get('kernel')->getProjectDir();
            $imageUrl = $root . '/assets/posters/' . $poster->getPosterName();
            $thumbUrl = $root . '/assets/thumbs/' . $poster->getPosterName();
            FileEngine::get()->compressImage($imageUrl, $thumbUrl, 180);
            return new BinaryFileResponse($thumbUrl);
        } else {
            return $poster != null ? $downloadHandler->downloadObject($poster, $field = 'posterFile', $className = null, $forceDownload = false) : $this->serializeData([
                'message' => 'Kitap Kapağı Bulunamadı',
                'error' => true
            ]);
        }
    }

    /**
    * @Route("/get/poster/{posterName}", name="get_poster")
    */
    public function getPoster($posterName, DownloadHandler $downloadHandler): StreamedResponse
    {
        $poster = $this->repo->findOneBy(['posterName' => $posterName]);
        return $downloadHandler->downloadObject($poster, $field = 'posterFile', $className = null, $forceDownload = false);
    }

    /**
    * @Route("/uploads/posters/{basename}", name="poster_uploads")
    */
    public function downloadUploadedPoster($basename, DownloadHandler $downloadHandler): StreamedResponse
    {
        $poster = $this->repo->findOneBy(['posterName' => $basename]);
        return $downloadHandler->downloadObject($poster, $field = 'posterFile', $className = null, $forceDownload = false);
    }
}