<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\Routing\Annotation\Route;

class AssetsController extends AbstractController
{
    /**
     * @Route("/public/js/{script}", name="app_scripts", methods={"GET"})
     */
    public function getJs($script): Response
    {
        $root = $this->container->get('kernel')->getProjectDir();
        $url = $root . '/assets/js/' . $script;
        return new BinaryFileResponse($url);
    }

}
