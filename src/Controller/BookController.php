<?php

namespace App\Controller;

use App\Entity\Book;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Contracts\Translation\TranslatorInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Vich\UploaderBundle\Handler\DownloadHandler;

class BookController extends GenericAbstractController
{
    public function __construct(EntityManagerInterface $entityManager = null, TranslatorInterface $translator = null)
    {
        if ($entityManager != null && $translator != null) {
            parent::__construct($entityManager, $translator);
        }
    }

    function class()
    {
        return Book::class;
    }

    function resource()
    {
        return 'books';
    }

    function fill(&$wireframe, $json)
    {
        $wireframe->setData($json['data'])
            ->setTitle($json['title'])
            ->setAuthor($json['author']);
    }

    protected function modifyList($list): array 
    {
        return self::modifyBookList($list);
    }

    public static function modifyBookList($list): array 
    {
        $emptyList = array_filter($list, function($el) {
            return is_null($el->getNext());
        });
        if ($last = reset($emptyList)) {
            $modified = array();
            for ($i=0; $i < count($list); $i++) { 
                foreach ($list as $book) {
                    $next = $book->getNext();
                    if(!is_null($next)) 
                    {
                        if ($next->getId() == $last->getId()) {
                            $modified[] = $last;
                            $last = $book;
                            break;
                        }
                    }
                }
            }
            $modified[] = $last;
            $modified = array_reverse($modified);
            foreach (array_slice($emptyList, 1) as $book) {
                $modified[] = $book;
            }
            return array_values(array_filter($modified, function($el) {
                return $el->getPublished();
            }));
        } else {
            return array_values(array_filter($list, function($el) {
                return $el->getPublished();
            }));
        }
    }

    /**
     * @Route("/index", name="books_index")
     */
    public function index(): Response
    {
        $list = $this->repo->findAll();
        return $this->render('book/index.html.twig', [
            'controller_name' => 'BookController',
            'data' => $list,
        ]);
    }

    /**
     * @Route("/download/book", name="book_download", methods={"POST"})
     */
    public function downloadBook(DownloadHandler $downloadHandler, Request $request): Response
    {
        $json = (array) $request->request->all();
        $book = $this->repo->findOneBy(['pdfName' => $json['pdfName']]);
        return $book != null ? $downloadHandler->downloadObject($book, $field = 'pdfFile', $className = null, $forceDownload = false) : $this->serializeData([
            'message' => 'Kitap Bulunamadı',
            'error' => true
        ]);
    }

    /**
     * @Route("/bundles/jjalvarezlpdfjsviewer/tmpPdf/{pdfName}", name="book_view")
     */
    public function viewBook($pdfName, DownloadHandler $downloadHandler): StreamedResponse
    {
        $book = $this->repo->findOneBy(['pdfName' => $pdfName]);
        return $downloadHandler->downloadObject($book, $field = 'pdfFile', $className = null, $forceDownload = false);
    }


    /**
    * @Route("/uploads/books/{basename}", name="book_uploads")
    */
    public function downloadUploadedBook($basename, DownloadHandler $downloadHandler): StreamedResponse
    {
        $book = $this->repo->findOneBy(['pdfName' => $basename]);
        return $downloadHandler->downloadObject($book, $field = 'pdfFile', $className = null, $forceDownload = false);
    }
}
