<?php

namespace App\Controller;

use App\Entity\Author;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\Translation\TranslatorInterface;
use Doctrine\ORM\EntityManagerInterface;

class AuthorController extends GenericAbstractController
{
    public function __construct(EntityManagerInterface $entityManager = null, TranslatorInterface $translator = null)
    {
        if ($entityManager != null && $translator != null) {
            parent::__construct($entityManager, $translator);
        }
    }

    function class()
    {
        return Author::class;
    }

    function resource()
    {
        return 'authors';
    }
    
    function fill(&$wireframe, $json)
    {
        $wireframe
            ->setFullName($json['full_name']);
    }
}
