<?php

namespace App\Controller;

use App\Engines\HttpEngine;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\HttpFoundation\Request;
use App\Engines\ReflectEngine;
use Symfony\Contracts\Translation\TranslatorInterface;
use Doctrine\ORM\EntityManagerInterface;

abstract class GenericController extends AbstractController
{
    protected $em;
    protected $translator;
    protected $repo;

    abstract protected function class();
    abstract protected function modifyList($list): array;
    abstract protected function resource();
    abstract protected function fill(&$wireframe, $json);

    public function __construct(EntityManagerInterface $entityManager, TranslatorInterface $translator)
    {
        $this->em = $entityManager;
        $this->translator = $translator;
        $this->repo = $this->em->getRepository($this->class());
    }

    protected function serializeData($data): Response
    {
        return HttpEngine::get()->serialize($data);
    }

    protected function toJson($data): string
    {
        return HttpEngine::get()->toJson($data);
    }

    public function list()
    {
        $list = $this->repo->findAll();
        $data = [
            'list' => $this->modifyList($list),
            'error' => false,
        ];
        return $this->serializeData($data);
    }

    public function single($id)
    {
        $single = $this->repo->findOneBy(['id' => $id]);
        $data = [
            'single' => $single,
            'error' => $single == null,
        ];
        return $this->serializeData($data);
    }

    public function insert(ValidatorInterface $validator, Request $request)
    {
        $json = (array) $request->request->all();
        $c = $this->class();
        $entity = new $c;
        $this->fill($entity, $json);

        $errors = $validator->validate($entity);
        $data = null;
        if (count($errors) > 0) {
            $data = [
                'message' => (string) $errors,
                'error' => true
            ];
        } else {
            try {
                $this->em->persist($entity);
                $this->em->flush();
                $data = [
                    'inserted' => $entity,
                    'message' => $this->translator->trans(
                        'item_generated',
                        ['item' => $this->translator->trans($this->resource())]
                    ),
                    'error' => false
                ];
            } catch (\Throwable $th) {
                $data = [
                    'message' => (string) $th,
                    'error' => true
                ];
            }
        }

        return $this->serializeData($data);
    }

    public function edit(Request $request, $id)
    {
        $json = (array) $request->request->all();
        $c = $this->class();
        $entity = new $c;
        $this->fill($entity, $json);
        $entity->setId($id);
        $this->em->persist($entity);
        $this->em->flush();
        $data = [
            'updated' => $entity,
            'message' => $this->translator->trans(
                'item_updated',
                ['item' => $this->translator->trans($this->resource())]
            ),
            'error' => false
        ];
        return $this->serializeData($data);
    }

    public function delete($id)
    {
        $entity = $this->em->getRepository($this->class())->find($id);
        $this->em->remove($entity);
        $this->em->flush();
        $data = [
            'deleted' => $entity,
            'message' => $this->translator->trans(
                'item_deleted',
                ['item' => $this->translator->trans($this->resource())]
            ),
            'error' => false
        ];
        return $this->serializeData($data);
    }
}
