<?php

namespace App\Controller;

use Symfony\Contracts\Translation\TranslatorInterface;
use Doctrine\ORM\EntityManagerInterface;

abstract class GenericAbstractController extends GenericController
{
    public function __construct(EntityManagerInterface $entityManager, TranslatorInterface $translator)
    {
        parent::__construct($entityManager, $translator);
    }

    protected function modifyList($list): array 
    {
        return $list;
    }
}
