<?php

namespace App\Controller;

use App\Entity\Category;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\Translation\TranslatorInterface;
use Doctrine\ORM\EntityManagerInterface;

class CategoryController extends GenericAbstractController
{
    public function __construct(EntityManagerInterface $entityManager = null, TranslatorInterface $translator = null)
    {
        if ($entityManager != null && $translator != null) {
            parent::__construct($entityManager, $translator);
        }
    }

    function class()
    {
        return Category::class;
    }

    function resource()
    {
        return 'categories';
    }
    
    function fill(&$wireframe, $json)
    {
        $parent = $this->container->get(CategoryRepository::class)->find($json['parent_id']);
        $wireframe
            ->setTitle($json['title'])
            ->setParent($parent);
    }
}
