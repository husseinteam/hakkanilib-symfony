<?php

namespace App\Engines;

use Symfony\Component\HttpFoundation\Response;
use JMS\Serializer\Serializer;
use JMS\Serializer\SerializationContext;

class HttpEngine
{
    /**
     *
     * @var HttpEngine
     */
    private static $instance;

    public static function get()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * @var Serializer
     */
    private $serializer;

    public function __construct()
    {
        $serializeBuilder = \JMS\Serializer\SerializerBuilder::create();
        $serializeBuilder->setPropertyNamingStrategy(new \JMS\Serializer\Naming\IdenticalPropertyNamingStrategy());
        $this->serializer = $serializeBuilder->build();
    }

    public function serialize($data, $statusCode = Response::HTTP_OK): Response
    {
        return new Response($this->toJson($data), $statusCode, ['Content-Type' => 'application/json']);
    }

    public function toJson($data): string
    {
        $jsonContent = $this->serializer->serialize($data, 'json', SerializationContext::create()->enableMaxDepthChecks());
        return $jsonContent;
    }
}