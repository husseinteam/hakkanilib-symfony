<?php

namespace App\Engines;

class ReflectEngine
{
    /**
     * @var ReflectEngine
     */
    private static $instance;

    public static function get()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    private function getType($property): string
    {
        if (preg_match(
            '/type="(.+?)"/',
            $property->getDocComment(),
            $matches
        )) {
            list(, $type) = $matches;
            return $type;
        } else if (preg_match(
            '/targetEntity="(.+?)"/',
            $property->getDocComment(),
            $matches
        )) {
            return 'object';
        }  else if (preg_match(
            '/targetEntity=(\w+)::(\w+),/',
            $property->getDocComment(),
            $matches
        )) {
            return 'object';
        } else {
            var_dump($property->getDocComment());
            return null;
        }
    }
    public function equalsObjectToArray(object $object, array $array, array $exclude = []): bool
    {
        $reflection = new \ReflectionClass($object);
        foreach ($reflection->getProperties() as $property) {
            $property->setAccessible(true);
            if (!in_array($this->getType($property), ['object', 'array'])) {
                $value = $property->getValue($object);
                $name = $property->getName();
                if (!in_array($name, $exclude) && $value != $array[$name]) {
                    return false;
                }
            }
        }
        return true;
    }

    public function objectToArray(object $object): array
    {
        $array = array();
        $reflection = new \ReflectionClass($object);
        foreach ($reflection->getProperties() as $property) {
            $property->setAccessible(true);
            if (!in_array($this->getType($property), ['object', 'array'])) {
                $name = $property->getName();
                $array[$name] = $property->getValue($object);
            }
        }
        return $array;
    }

    public function arrayToObject(array $array, $class, array $exclude = []): object
    {
        $object = new $class();
        $reflection = new \ReflectionClass($object);
        foreach ($reflection->getProperties() as $property) {
            $name = $property->getName();
            if (!in_array($name, $exclude)) {
                $property->setAccessible(true);
                if (!in_array($this->getType($property), ['object', 'array'])) {
                    $property->setValue($object, $array[$name]);
                }
            }
        }
        return $object;
    }
}
