<?php

namespace App\Tests\ControllerTests;

use App\Engines\ReflectEngine;
use Symfony\Component\Dotenv\Dotenv;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;

abstract class GenericControllerTest extends WebTestCase
{
    abstract protected function wireframe(): object;
    abstract protected function editors(): array;
    abstract protected function editProps(): array;
    abstract protected function referenceProps(): array;
    abstract protected function exclusionProps(): array;
    abstract protected function controller();

    protected $book_path;
    protected $poster_path;

    private static $env;
    private static $translator;
    private static $em;
    /**
     * @var KernelBrowser
     */
    private static $client;

    public static function setUpBeforeClass()
    {
        if (self::$client == null) {
            self::$client = self::createClient();
        }
        self::bootKernel();
        $dotenv = new Dotenv();
        $dotenv->loadEnv('./.env.test');
        self::$em = self::$container->get('doctrine.orm.entity_manager');
        self::$translator = self::$container->get('translator');
        self::$env = $_ENV;
    }

    public static function env(string $key): string
    {
        return self::$env[$key];
    }

    private function entity(): string
    {
        $c = $this->controller();
        $instance = new $c();
        return $instance->class();
    }
    private function resource(): string
    {
        $c = $this->controller();
        $instance = new $c();
        return $instance->resource();
    }

    private function tokenize(array $credentials): string
    {
        $response = $this->clientResponse('POST', 'login', $credentials, true);
        $this->assertEquals(200, $response->getStatusCode());
        $json = json_decode($response->getContent(), true);
        return $json['token'];
    }

    private function clientResponse(string $method, string $path, array $data = [], bool $login = false)
    {
        $server = $this->env('SERVER');
        $resource = $login ? 'auth' : $this->resource();
        $uri = "{$server}/{$resource}/{$path}";
        self::$client->request(
            $method, $uri, $data, [],
            $login ? [
                'CONTENT_TYPE' => 'application/json',
            ] : [
                'CONTENT_TYPE' => 'application/json',
                'HTTP_X_AUTH_TOKEN' => $this->tokenize([
                    'identity' => '25871077074',
                    'password' => '293117',
                ]),
            ],
        );
        return self::$client->getResponse();
    }

    private function editWireFrame(): object
    {
        self::bootKernel();
        $wireframe = $this->wireframe();
        $editProps = $this->editProps();
        foreach ($this->editors() as $prop => $editor) {
            if ($prop !== 'id') {
                $wireframe = $editor($wireframe, $editProps[$prop]);
            }
        }
        return $wireframe;
    }

    public function testInsert()
    {
        self::bootKernel();
        $data = ReflectEngine::get()->objectToArray($this->wireframe());
        $data = array_merge($data, $this->referenceProps());
        $response = $this->clientResponse('POST', 'insert', $data);
        $this->assertEquals(200, $response->getStatusCode());

        $json = json_decode($response->getContent(), true);
        $this->assertTrue(ReflectEngine::get()
            ->equalsObjectToArray($this->wireframe(), $json['inserted'], array_merge(['id'], $this->exclusionProps())));
        $this->assertFalse($json['error']);
        $this->assertEquals(
            $json['message'],
            self::$translator->trans(
                'item_generated',
                ['item' => self::$translator->trans($this->resource())]
            )
        );
    }

    public function testList()
    {
        $response = $this->clientResponse('GET', 'list');
        $this->assertEquals(200, $response->getStatusCode());

        $json = json_decode($response->getContent(), true);
        $this->assertGreaterThan(0, count($json['list']));
    }

    public function testEdit()
    {
        $wireframe = $this->editWireFrame();
        $lastWireframeId = self::$em->getRepository($this->entity())
            ->findOneBy([], ['id' => 'desc'])->getId();
        $data = ReflectEngine::get()->objectToArray($wireframe);
        $data = array_merge($data, $this->referenceProps());
        $response = $this->clientResponse('POST', "{$lastWireframeId}/edit", $data);
        $this->assertEquals(200, $response->getStatusCode());

        $json = json_decode($response->getContent(), true);
        $this->assertTrue(ReflectEngine::get()
            ->equalsObjectToArray($wireframe, $json['updated'], array_merge(['id'], $this->exclusionProps())));
        $this->assertFalse($json['error']);
        $this->assertEquals(
            $json['message'],
            self::$translator->trans(
                'item_updated',
                ['item' => self::$translator->trans($this->resource())]
            )
        );
    }

    public function testDelete()
    {
        $lastWireframeId = self::$em->getRepository($this->entity())
            ->findOneBy([], ['id' => 'desc'])->getId();
        $response = $this->clientResponse(
            'POST',
            "{$lastWireframeId}/delete"
        );
        $this->assertEquals(200, $response->getStatusCode());

        $json = json_decode($response->getContent(), true);
        $this->assertFalse($json['error']);
        $this->assertEquals(
            $json['message'],
            self::$translator->trans(
                'item_deleted',
                ['item' => self::$translator->trans($this->resource())]
            )
        );
    }
}
