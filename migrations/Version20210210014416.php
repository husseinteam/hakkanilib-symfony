<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210210014416 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE book ADD next_id INT DEFAULT NULL, ADD rtl TINYINT(1) NOT NULL');
        $this->addSql('ALTER TABLE book ADD CONSTRAINT FK_CBE5A331AA23F6C8 FOREIGN KEY (next_id) REFERENCES book (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_CBE5A331AA23F6C8 ON book (next_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE book DROP FOREIGN KEY FK_CBE5A331AA23F6C8');
        $this->addSql('DROP INDEX UNIQ_CBE5A331AA23F6C8 ON book');
        $this->addSql('ALTER TABLE book DROP next_id, DROP rtl');
    }
}
